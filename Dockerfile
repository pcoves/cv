FROM archlinux

RUN pacman -Syu --noconfirm texlive-core texlive-fontsextra texlive-latexextra && \
  rm -rf /var/cache/pacman/pkg/*
