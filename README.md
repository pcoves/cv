# CV

## Build

```
git clone https://gitlab.com/pcoves/cv.git && cd cv
git submodule update --init
pdflatex cvPabloCOVES.tex
```

## Use

Published version available [here](https://gitlab.com/api/v4/projects/34610672/packages/generic/cv/latest/cvPabloCOVES.pdf)
